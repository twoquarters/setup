var expanding = true;
var w = 0;
var offset = 3.0;
var scaleVariable = getScaleVar();

function getScaleVar() {
	var query = window.location.href.split('?')[1];
	var params = query.split('=');
  var pair = params[1].split('=');

	return Number(pair[0]);
}

function setup() {
  createCanvas(600 * scaleVariable, 600 * scaleVariable);
	frameRate(3);
  rectMode(CENTER);
  background(50);
}

function draw() {
  scale(scaleVariable);
	fill(35, random(150, 200), 45);
  rect(width / (scaleVariable * 2), height / (scaleVariable * 2), 100, 100);
}
