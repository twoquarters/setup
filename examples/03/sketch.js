var scaleVariable = getScaleVar();

var posX = 0;
var posY = 0;

function getScaleVar() {
	var query = window.location.href.split('?')[1];
	var params = query.split('=');
	var pair = params[1].split('=');

	return Number(pair[0]);
}

var colorValue = 0;

function setup() {
	createCanvas(600 * scaleVariable, 600 * scaleVariable);
	rectMode(CENTER);
	frameRate(120);
	fill(colorValue);
	background(140);

	posX = (width / 2) / scaleVariable;
	posY = (width / 2)  / scaleVariable;
}

var randomX;
var randomY;

var increasing = true;

function draw() {
	scale(scaleVariable);

	if (increasing) {
		colorValue += 5;
		if (colorValue >= 255) {
			increasing = false;
			colorValue = 255;
		}
	} else {
		colorValue -= 5;
		if (colorValue <= 0) {
			increasing = true;
			colorValue = 0;
		}
	}

	fill(colorValue);

	for(var i = 0; i < 20; i++)
	{
		randomX = random(-6, 6);
		randomY = random(-6, 6);

		while (!inBounds(posX + randomX, posY + randomY))
		{
			randomX = random(-20, 20);
			randomY = random(-20, 20);
		}

		posX += randomX;
		posY += randomY;

		ellipse(posX, posY, 5, 5);
	}
}

function inBounds(checkX, checkY) {
	if (checkX > 0 && checkY > 0 && checkX < (1200 * scaleVariable) && checkY < (1200 * scaleVariable)) {
		return true;
	}
	else {
		return false;
	}
}
