var scaleVariable = getScaleVar();

function getScaleVar() {
	var query = window.location.href.split('?')[1];
	var params = query.split('=');
  var pair = params[1].split('=');

	return Number(pair[0]);
}

function setup() {
	createCanvas(600 * scaleVariable, 600 * scaleVariable);
  rectMode(CENTER);
  frameRate(2);

	background(125);
}

function draw() {
  scale(scaleVariable);

  for (var i = 0; i < 10; i++) {
    fill(random(100, 255));
    ellipse(random(50, 550), random(50, 550), 50, 50);
  }
}
