var scaleVariable = getScaleVar();

function getScaleVar() {
	var query = window.location.href.split('?')[1];
	var params = query.split('=');
  var pair = params[1].split('=');

	return Number(pair[0]);
}

function setup() {
  createCanvas(600 * scaleVariable, 600 * scaleVariable);
  rectMode(CENTER);
  frameRate(1);
}

var red = false;
var greenIn;

function draw() {
  background(0);

	greenIn = parseInt(random(1, 14)) * 50;

  scale(scaleVariable);

  for (var x = 0; x < 700; x+= 50) {
    for (var y = 0; y < 700; y+= 50) {
      red = !red;

			if (y == greenIn && x == greenIn) {
				fill (50, 120, 100);
			} else {
				if (red) {
	        fill (50, 70, 200);
	      } else {
	        fill(50, 100, 200);
	      }
			}
      ellipse(x, y, 100, 100);
    }
  }
  red = !red;
}
