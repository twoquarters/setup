var scaleVariable = getScaleVar();

function getScaleVar() {
	var query = window.location.href.split('?')[1];
	var params = query.split('=');
  var pair = params[1].split('=');

	return Number(pair[0]);
}

function setup() {
	createCanvas(600 * scaleVariable, 600 * scaleVariable);
  rectMode(CENTER);
  frameRate(2);

	background(75);

	initialX1 = random(0, 100);
	initialX2 = random(400, 600);
	initialY1 = random(0, 100);
	initialY2 = random(400, 600);
}

var initialX1, initialX2, initialY1, initialY2;

function draw() {
  scale(scaleVariable);

  for (var i = 0; i < 50; i++) {
    fill(random(255), random(255), random(255));
    ellipse(random(initialX1, initialX2), random(initialY1, initialY2), 10, 10);
  }
}
