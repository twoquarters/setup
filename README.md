# Gallery Two Quarters #

## This website was made for the second WWW Authoring Assignment by team 24

##Authors:
- Dumitru Vulpe
- Marius Urbelis
- Stanislav Turcanu
- Patrik Turton-Smith

##Description:
There are few places online where content creators can interact directly with their chosen delivery method, where their creativity is seen exactly as they intended. Completely unaltered and unchanged.  

We want Two Quarters to be that website, and what we have working today is the building blocks for that platform.  

Two Quarters will allow users to create designs directly onto the webpage, and share them immediately. Our website is the canvas that artists can draw onto, and the gallery at which they showcase their work to the world at the same time. Creators will not only be able to get their content seen by people around the world, but also inspire others to create something new, in turn continuing this cycle.  

For now, this website just hosts some computer-generated art which is different every time the page is loaded. And in the future, we will slowly start adding different bits which will allow us to build a full-fledged site where users will be able to upload their own sketches.  